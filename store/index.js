import moment from "moment";

export const state = () => ({
  news: [],
  loadedNews: [],
  currentNews: {},
  currentPage: 1,
  pageLimit: 5,
  paginationLength: 0,
  dateFilterItem: null,
  titleFilterItem: null,
  currentImageIndex: 0,
})

export const mutations = {
  SET_LOADED_NEWS(state, data) {
    state.loadedNews = data
  },
  FILTER_NEWS(state) {
    let news = this.$filterByDate(state.loadedNews, "date", state.dateFilterItem);
    news = this.$filterByText(news, "title", state.titleFilterItem);
    state.paginationLength = this.$paginationLength(news, state.pageLimit);
    state.news = this.$paginate(news, state.currentPage, state.pageLimit);
  },
  SET_DATE_FILTER(state, data) {
    state.dateFilterItem = data;
  },
  SET_TITLE_FILTER(state, data) {
    state.titleFilterItem = data;
  },
  SET_CURRENT_PAGE(state, data) {
    state.currentPage = data;
  },
  SET_CURRENT_NEWS_BY_GUID(state, data) {
    state.currentNews = this.$findBy(state.loadedNews, "guid", data);
  },
  SET_CURRENT_IMAGE_INDEX(state, data) {
    state.currentImageIndex = data;
  }
}

export const actions = {
  async nuxtServerInit({ commit }) {
    const data  = await this.$getNewsList();
    
    data.items.forEach(item => {
      item.date = moment(item.pubDate).valueOf();
      moment.locale("ru");
      item.formatedDate = moment(item.pubDate).format('llll');
      // item.formatedDate = moment(item.pubDate).format("DD.MM.YYYY");
    });

    const items = this.$sortDesc(data.items, "date");
    
    commit("SET_LOADED_NEWS", items);
    commit("FILTER_NEWS");
  }
}