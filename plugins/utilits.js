import filterUtilit from '~/utilits/filter';
import paginationUtilit from '~/utilits/pagination';
import sortUtilit from '~/utilits/sort';

export default ({  }, inject) => {
  const allMethods = {
    ...filterUtilit(),
    ...paginationUtilit(),
    ...sortUtilit(),
    // import another utilits here
  }

  const methods = Object.keys(allMethods)
  methods.forEach((method) => {
    inject(method, allMethods[method])
  })
}