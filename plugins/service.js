import Parser from "rss-parser";
const parser = new Parser({
  customFields: {
    item: [
      ['enclosure', 'media', {keepArray: true}],
    ]
  }
});

export default ({ }, inject) => {
    inject("getNewsList", async () =>  await parser.parseURL(`http://static.feed.rbc.ru/rbc/logical/footer/news.rss`))
}