import moment from "moment";

export default () => { 
    return {
        filterByDate: (list, column, data) => {
            if (data === null) {
               return list;
            }
            return list.filter((item) => {
                return moment(item[column]).isSame(data, 'day');
            });
        },
        filterByText: (list, column, data) => {
            if (data === null) {
                return list;
            }
            return list.filter((item) => { 
                return item[column].toLowerCase().indexOf(data.toLowerCase()) > -1 
            });
        },
        findBy: (list, column, data) => {
            if (data === null) {
                return {};
            }
            return list.find((item) => {
                return item[column] === data; 
            });
        },
        // more verbs and more actions
    }
}