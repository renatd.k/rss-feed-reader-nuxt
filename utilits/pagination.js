export default () => {
    return {
        paginate: (list, page, limit) => {
            return list.slice((page - 1) * limit, page * limit);
        },
        paginationLength: (list, limit) => {
            return Math.ceil(list.length / limit);
        },
        // more verbs and more actions
    }
}