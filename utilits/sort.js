export default () => {
    return {
        sortAsc: (list, column) => {
            return list.sort((a,b) => a[column] - b[column]);
        },
        sortDesc: (list, column) => {
            return list.sort((a,b) => b[column] - a[column]);
        }
    }
    // more verbs and more actions
}