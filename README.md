# rss-feed-reader-nuxt

# Задача
### Необходимо разработать приложение по выводу новостной ленты
[news.rss](http://static.feed.rbc.ru/rbc/logical/footer/news.rss.)

### Требования к оформлению страницы:

1. Шапка с заголовком приложения.
2. Блок с фильтрами по дате и совпадению слов в заголовке новостей.
3. Список новостей:
    1. Сортировка от более свежих к более старым
    2. Каждая новость выводится в виде указания даты, заголовка и анонса.
    3. Заголовок новости кликабелен и ведет на отдельную страницу с конкретной новостью.
    4. Лимит новостей на странице: не больше 5 штук.
4. Блок с пагинацией.

### Требования к оформлению страницы с конкретной новостью:

1. Заголовок
2. Блок с датой публикации и именем автора (если есть).
3. Анонс новости.
4. Список фотографий (если они есть). Вывод в виде слайдера с возможностью
просмотра увеличенных изображений в попапе.
5. Ссылка на оригинальную новость на сайте РБК.

### Требования:
1. Стек: Vue.js, Nuxt.js, Vue Router.
2. Визуальная часть на свое усмотрение. Плюсом будет использование
библиотеки Vuetify или Element.
# Результат

![alt Screen 1](screenshots/screen_1.png)
![alt Screen 2](screenshots/screen_2.png)
![alt Screen 3](screenshots/screen_3.png)
![alt Screen 4](screenshots/screen_4.png)
![alt Screen 5](screenshots/screen_5.png)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
